package nl.yourilefers.android.saxion.contactlistactivity;

import java.util.Observable;
import java.util.Observer;

import nl.yourilefers.android.saxion.contactlistactivity.model.Model;
import nl.yourilefers.android.saxion.contactlistactivity.models.Contact;
import nl.yourilefers.android.saxion.contactlistactivity.view.ContactAdapter;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

public class ActivityMain extends Activity implements Observer {
	
	// The main listview
	private ListView lv_contact;
	
	// The current model
	private Model model;
	
	// The adapter
	private ContactAdapter adapter;
	
	@Override
	protected void onCreate( Bundle savedInstanceState ) {
		
		super.onCreate( savedInstanceState );
		setContentView( R.layout.main );
		
		// Set model
		this.model = Model.getInstance( this );
		
		// Register observer
		this.model.addObserver( this );
		
		// Get the listview
		this.lv_contact = ( ListView ) findViewById( R.id.lv_contacts );
		
		// Create new adapter
		this.adapter = new ContactAdapter( this , R.layout.listitem , this.model.getContacts() );
		
		// Set adapter
		this.lv_contact.setAdapter( adapter );
		
		// Set OnItemClickListener
		this.lv_contact.setOnItemClickListener( new OnItemClickListener() {

			@Override
			public void onItemClick( AdapterView<?> av , View v , int pos ,
					long id ) {
				
				// Create intent
				Intent intent = new Intent( ActivityMain.this , ActivityDetail.class );
				
				// Add id of the contact
				intent.putExtra( "id" , ( ( Contact ) av.getItemAtPosition( pos ) ).getId() );
				
				// Start activity
				startActivity( intent );
				
			}
			
		} );
		
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}
	
	@Override
	public void update(Observable observable, Object data) {
		
		// Notify adapter
		this.adapter.notifyDataSetChanged();
		
	}
	
}
