package nl.yourilefers.android.saxion.contactlistactivity;

import java.io.IOException;
import java.io.InputStream;

import nl.yourilefers.android.saxion.contactlistactivity.model.Model;
import nl.yourilefers.android.saxion.contactlistactivity.models.Contact;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

public class ActivityDetail extends Activity {
	private static final int CAMERA_PIC_REQUEST = 1337;
	
	// The model instance
	private Model model;
	
	// The current contact
	private Contact contact = null;
	
	// The firstname EditText
	private EditText et_firstName;
	
	// The lastname EditText
	private EditText et_lastName;
	
	// The phonenumber EditText
	private EditText et_phoneNumber;
	
	// The location EditText
	private EditText et_location;
	
	// The image view
	private ImageView iv_image;
	
	// If false, watchers will not change
	private boolean watch = true;
	
	@Override
	protected void onCreate( Bundle savedInstanceState ) {
		
		super.onCreate( savedInstanceState );
		setContentView( R.layout.detail );
		
		// Get an instance of the model
		this.model = Model.getInstance( this );
		
		// Find views
		this.et_firstName 	= ( EditText ) findViewById( R.id.et_firstName );
		this.et_lastName 	= ( EditText ) findViewById( R.id.et_lastName );
		this.et_phoneNumber = ( EditText ) findViewById( R.id.et_phoneNumber );
		this.et_location 	= ( EditText ) findViewById( R.id.et_location );
		this.iv_image 		= ( ImageView ) findViewById( R.id.iv_image );
		
		try {
			
			// Try to get intent
			this.contact = this.model.getContact( getIntent().getIntExtra( "id" , -1 ) );
			
			if( this.contact != null ) {
				
				// Load data
				loadData();
				
			}
			
		} catch( NullPointerException e ) {
			
			Log.e( "ActivityDetail" , "Could not load a contact: nullpointer -> " + e.getLocalizedMessage() );
			
		}
		
		// Create TextWatcher
		Watcher watcher = new Watcher();
		
		// Add listeners
		this.et_firstName.addTextChangedListener( watcher );
		this.et_lastName.addTextChangedListener( watcher );
		this.et_phoneNumber.addTextChangedListener( watcher );
		this.et_location.addTextChangedListener( watcher );
		
		// Set listener for onClick event for image
		this.iv_image.setOnClickListener( new OnClickListener() {

			@Override
			public void onClick(View v) {
				
				// Create intent
				Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);

			    // start the image capture Intent
			    startActivityForResult( intent, CAMERA_PIC_REQUEST );
				
			}
			
		} );
		
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	    InputStream stream = null;
	    
	    if (requestCode == CAMERA_PIC_REQUEST) {
	    	
	        if (resultCode == RESULT_OK) {
	        	
	            // Image captured and saved to fileUri specified in the Intent
	            //Toast.makeText(this, "Image saved to:\n" + data.getData(), Toast.LENGTH_LONG).show();

	            try {
	            	
	            	// Get image
	                Bitmap bitmap = ( Bitmap ) data.getExtras().get( "data" );
		            
		            // Change image
		            this.model.getContact( this.contact.getId() ).setImage( new BitmapDrawable( getResources() , bitmap ) );
					
					// Notify
					this.model.notif();
					
					// Reload contact
					this.contact = this.model.getContact( this.contact.getId() );
					
					// Reload data
					loadData();
		            
				} catch ( NullPointerException e ) {
					
					Toast.makeText( this , "Could not save image" , Toast.LENGTH_LONG ).show();
					
				} finally {
					
					if (stream != null) {
						
						try {
			                stream.close();
			            } catch ( IOException e ) {
			                e.printStackTrace();
			            }
					
					}
	            	
			    }
	            
	        }
	        
	    }
	    
	}
	
	@Override
	public boolean onCreateOptionsMenu( Menu menu ) {
		
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate( R.menu.activity_detail , menu );
		return true;
		
	}
	
	@Override
	public boolean onOptionsItemSelected( MenuItem item ) {
		
		switch ( item.getItemId() ) {
			
			case android.R.id.home:
				NavUtils.navigateUpFromSameTask( this );
				return true;
			
		}
		return super.onOptionsItemSelected( item );
		
	}
	
	private void loadData() {
		
		watch = false;
		
		// Set firstname
		this.et_firstName.setText( this.contact.getFirstName() );
		
		// Set lastname
		this.et_lastName.setText( this.contact.getLastName() );
		
		// Set phone number
		this.et_phoneNumber.setText( this.contact.getPhoneNumber() );
		
		// Set location
		this.et_location.setText( this.contact.getLocation() );
		
		// Set image
		this.iv_image.setImageDrawable( this.contact.getImage() );
		
		// Set actionbar
		setActionBarData();
		
		watch = true;
		
	}
	
	private void setActionBarData() {
		
		// Set actionbar titles
		this.getActionBar().setTitle( this.contact.getName() );
		this.getActionBar().setSubtitle( this.contact.getPhoneNumber() );
		this.getActionBar().setIcon( this.contact.getImage() );
		
	}
	
	private class Watcher implements TextWatcher {

		@Override
		public void afterTextChanged(Editable s) {
			
			if( watch ) {
				
				// Update values
				model.getContact( contact.getId() ).setFirstName( et_firstName.getText().toString() );
				model.getContact( contact.getId() ).setLastName( et_lastName.getText().toString() );
				model.getContact( contact.getId() ).setPhoneNumber( et_phoneNumber.getText().toString() );
				model.getContact( contact.getId() ).setLocation( et_location.getText().toString() );
				
				// Notify
				model.notif();
				
				// Reload contact
				contact = model.getContact( contact.getId() );
				
				// Reload actionbar
				setActionBarData();
				
			}
			
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {}

		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {}
		
	}
	
}
