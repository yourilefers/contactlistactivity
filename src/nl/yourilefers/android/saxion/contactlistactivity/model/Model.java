package nl.yourilefers.android.saxion.contactlistactivity.model;

import java.util.ArrayList;
import java.util.Observable;

import nl.yourilefers.android.saxion.contactlistactivity.R;
import nl.yourilefers.android.saxion.contactlistactivity.models.Contact;
import android.content.Context;

public class Model extends Observable {
	
	// The current Model instance
	private static Model model = null;
	
	// ArrayList containing all contact
	private ArrayList<Contact> contacts;
	
	// The current context
	private Context context;
	
	// The private constructor for the model
	private Model( Context context ) {
		
		// Create arraylist
		this.contacts = new ArrayList<Contact>();
		
		// Set context
		this.context = context;
		
		// Add contacts
		contacts.add( new Contact( "Youri" , "Lefers" , "0642687386" , "Losser" , context.getResources().getDrawable( R.drawable.ic_launcher ) ) );
		contacts.add( new Contact( "Joost" , "van Dijk" , "0673859223" , "Oldenzaal" , context.getResources().getDrawable( R.drawable.ic_launcher ) ) );
		contacts.add( new Contact( "Ruben" , "Markvoort" , "0673869274" , "Enschede" , context.getResources().getDrawable( R.drawable.ic_launcher ) ) );
		contacts.add( new Contact( "Kevin" , "Holsink" , "0612983564" , "Ootmarsum" , context.getResources().getDrawable( R.drawable.ic_launcher ) ) );
		contacts.add( new Contact( "Ruud" , "Greven" , "1234567890" , "Enschede" , context.getResources().getDrawable( R.drawable.ic_launcher ) ) );
		contacts.add( new Contact( "Jantje" , "Smit" , "1230984567" , "Ergens" , context.getResources().getDrawable( R.drawable.ic_launcher ) ) );
		
	}
	
	/**
	 * Get the current instance of the model
	 * @return
	 */
	public static Model getInstance( Context context ) {
		
		if( model == null )
			model = new Model( context );
		
		return model;
		
	}
	
	/**
	 * Get an ArrayList<Contact> containing all contacts
	 * @return
	 */
	public ArrayList<Contact> getContacts() {
		return this.contacts;
	}
	
	/**
	 * Get the model with the given id
	 * @param id
	 * @return
	 */
	public Contact getContact( int id ) {
		
		for( Contact contact : this.contacts ) {
			if( contact.getId() == id ) {
				return contact;
			}
		}
		return null;
		
	}
	
	/**
	 * Notify observers of changes
	 */
	public void notif() {
		
		this.setChanged();
		this.notifyObservers();
		
	}
	
}
