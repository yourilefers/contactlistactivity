package nl.yourilefers.android.saxion.contactlistactivity.view;

import java.util.ArrayList;

import nl.yourilefers.android.saxion.contactlistactivity.R;
import nl.yourilefers.android.saxion.contactlistactivity.model.Model;
import nl.yourilefers.android.saxion.contactlistactivity.models.Contact;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class ContactAdapter extends ArrayAdapter<Contact> {
	
	// The current model
	private Model model;
	
	// The current context
	private Context context;
	
	// The list of contacts
	private ArrayList<Contact> contacts;
	
	public ContactAdapter( Context context , int resID , ArrayList<Contact> contacts ) {
		
		super( context , resID , contacts );
		
		// Get a model instance
		this.model = Model.getInstance( context );
		
		// Set context
		this.context = context;
		
		// Set contacts
		this.contacts = contacts;
		
	}
	
	/**
	 * Get number of items in list
	 */
	@Override
	public int getCount() {
		return this.contacts.size();
	}
	
	/**
	 * Get the task at the given position
	 */
	@Override
	public Contact getItem( int pos ) {
		return this.contacts.get( pos );
	}
	
	/**
	 * Get the position of a given task
	 */
	@Override
	public int getPosition( Contact contact ) {
		return this.contacts.indexOf( contact );
	}
	
	@Override
	public View getView( int position , View convertView , ViewGroup parent ) {
		
		// Create a viewholder
		final ViewHolder holder = new ViewHolder();
		
		// Inflate view
		View v = convertView;
		
		// Set the view if empty
		if( v == null || holder == null || holder.contact == null ) {
			
			// Save position
			holder.position = position;
			
			// Get the current task
			holder.contact = getItem( holder.position );
			
			// Get inflater service
			LayoutInflater inflater = ( LayoutInflater ) super.getContext()
		        .getSystemService( Context.LAYOUT_INFLATER_SERVICE );
			
			// Set row view
		    v = inflater.inflate( R.layout.listitem , null );
			
			// Set views
			holder.name = ( TextView ) v.findViewById( R.id.tv_name );
			holder.phonenumber = ( TextView ) v.findViewById( R.id.tv_phonenumber );
			holder.image = ( ImageView ) v.findViewById( R.id.iv_image );
			holder.phone = ( ImageView ) v.findViewById( R.id.iv_phone );
			
			// Set the tag
			v.setTag( holder );
			
		}
		
		// Set name
		holder.name.setText( holder.contact.getName() );
		
		// Set phonenumber
		holder.phonenumber.setText( holder.contact.getPhoneNumber() );
		
		// Set image
		holder.image.setImageDrawable( holder.contact.getImage() );
		
		// Set onClickListener for phone image
		holder.phone.setOnClickListener( new OnClickListener() {

			@Override
			public void onClick(View v) {
				
				// Create intent
				Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse( "tel:" + holder.contact.getPhoneNumber() ));
				
				// Start activity
				context.startActivity( intent );
				
			}
			
		} );
		
		return v;
		
	}
	
	static class ViewHolder {
		
		// The views
		TextView name;
		TextView phonenumber;
		ImageView image;
		ImageView phone;
		
		int position;
		Contact contact;
		
	}
	
}
