package nl.yourilefers.android.saxion.contactlistactivity.models;

import android.graphics.drawable.Drawable;

public class Contact {
	
	// All ids
	private static int IDS = 0;
	
	// ID of the contact
	private int id;

	// The first name
	private String firstName;
	
	// The last name
	private String lastName;
	
	// The phone number
	private String phoneNumber;
	
	// The location
	private String location;
	
	// The image of the person
	private Drawable image; 
	
	/**
	 * Create a contact object.
	 * 
	 * @param firstName
	 * @param lastName
	 * @param phoneNumber
	 * @param image
	 */
	@SuppressWarnings("static-access")
	public Contact( String firstName , String lastName , String phoneNumber , String location , Drawable image ) {
		
		// Set first name
		this.firstName = firstName;
		
		// Set last name
		this.lastName = lastName;
		
		// Set phone number
		this.phoneNumber = phoneNumber;
		
		// Set location
		this.location = location;
		
		// Set image
		this.image = image;
		
		// Set id
		this.id = this.IDS;
		this.IDS++;
		
	}
	
	/**
	 * Get the first name of the contact
	 * @return
	 */
	public String getFirstName() {
		return this.firstName;
	}
	
	/**
	 * Get the last name of the contact
	 * @return
	 */
	public String getLastName() {
		return this.lastName;
	}
	
	/**
	 * Get the entire name of the contact (first + last)
	 * @return
	 */
	public String getName() {
		return this.firstName + " " + this.lastName;
	}
	
	/**
	 * Get the phone number of the contact
	 * @return
	 */
	public String getPhoneNumber() {
		return this.phoneNumber;
	}
	
	/**
	 * Get the location of the contact
	 * @return
	 */
	public String getLocation() {
		return this.location;
	}
	
	/**
	 * Get the image of the contact
	 * @return
	 */
	public Drawable getImage() {
		return this.image;
	}
	
	/**
	 * Get the id of the contact
	 * @return
	 */
	public int getId() {
		return this.id;
	}
	
	/**
	 * Change the first name of the contact
	 * @param name
	 */
	public void setFirstName( String name ) {
		this.firstName = name;
	}
	
	/**
	 * Change the last name of the contact
	 * @param name
	 */
	public void setLastName( String name ) {
		this.lastName = name;
	}
	
	/**
	 * Change the phone number of the contact
	 * @param number
	 */
	public void setPhoneNumber( String number ) {
		this.phoneNumber = number;
	}
	
	/**
	 * Change the location of the contact
	 * @param location
	 */
	public void setLocation( String location ) {
		this.location = location;
	}
	
	/**
	 * Change the image of the contact
	 * @param image
	 */
	public void setImage( Drawable image ) {
		this.image = image;
	}
	
}
